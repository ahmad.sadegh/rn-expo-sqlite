import React, { useState } from 'react';
import { ScrollView, StyleSheet, Text, TextInput, View  } from 'react-native';
import Constants from 'expo-constants';
import { db } from './db';
import { Items } from "./Items";

export default function App() {
  const [text, setText] = React.useState(null)
  const [forceUpdate, forceUpdateId] = useForceUpdate()
  const [SelectedChild, setSelectedChild] = React.useState(6);

  React.useEffect(() => {
    db.transaction(tx => {
      tx.executeSql(
        "create table if not exists items (id integer primary key not null, done int, value text);"
      );
      tx.executeSql(
        "create table if not exists itemDetails (id integer primary key not null, parentId int, value text);"
      );
    });
  }, []);

  const changeSelected = (id) => {
    // Alert.alert('id : ',id.toString() )
    setSelectedChild(id);
    forceUpdate();//baes mishe index component items avaz she va reload konash
  }

  const add = (text) => {
    // is text empty?
    if (text === null || text === "") {
      return false;
    }

    db.transaction(
      tx => {
        tx.executeSql("insert into items (done, value) values (0, ?)", [text]);
        tx.executeSql("select * from items", [], (_, { rows }) =>
          console.log(JSON.stringify(rows))
        );
      },
      null,
      forceUpdate
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>SQLite Example</Text>
      <View style={styles.flexRow}>
        <TextInput
          onChangeText={text => setText(text)}
          onSubmitEditing={() => {
            add(text);
            setText(null);
          }}
          placeholder="what do you need to do?"
          style={styles.input}
          value={text}
        />
      </View>
      <ScrollView style={styles.listArea}>
        <Items
          key={`forceupdate-todo-${forceUpdateId}`}
          forDetail={false}
          selectedItem={SelectedChild}
          onChangeSelected={(id)=>changeSelected(id)}
          onPressItem={id =>
            db.transaction(
              tx => {
                tx.executeSql(`update items set done = 1 where id = ?;`, [
                  id
                ]);
              },
              null,
              forceUpdate
            )
          }
        />
        <Items
          forDetail
          key={`forceupdate-done-${forceUpdateId}`}
          selectedItem={SelectedChild || 1}
          onChangeSelected={(id)=>changeSelected(id)}
        // onPressItem={id =>
        //   db.transaction(
        //     tx => {
        //       tx.executeSql(`delete from items where id = ?;`, [id]);
        //     },
        //     null,
        //     forceUpdate
        //   )
        // }
        />
      </ScrollView>
    </View>
  );

}

function useForceUpdate() {
  const [value, setValue] = useState(0);
  return [() => setValue(value + 1), value];//khorujish hook forceupdate mesle hookhaye dige usestate ,..
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    paddingTop: Constants.statusBarHeight
  },
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  flexRow: {
    flexDirection: "row"
  },
  input: {
    borderColor: "#4630eb",
    borderRadius: 4,
    borderWidth: 1,
    flex: 1,
    height: 48,
    margin: 16,
    padding: 8
  },
  listArea: {
    backgroundColor: "#f0f0f0",
    flex: 1,
    paddingTop: 16
  },
  sectionContainer: {
    marginBottom: 16,
    marginHorizontal: 16
  },
  sectionHeading: {
    fontSize: 18,
    marginBottom: 8
  }
});