import React from 'react';
import { Text, TouchableOpacity, View, StyleSheet, TextInput} from 'react-native';
import Constants from 'expo-constants';
// import { styles } from './App';
import { db } from './db';

export function Items({ forDetail, onPressItem, selectedItem,onChangeSelected }) {
  const [items, setItems] = React.useState(null);
  const [detailText, setDetailText] = React.useState(null)
  // const [Selected, setIsSelected] = React.useState(6);

  React.useEffect(() => {
    console.log('selectedIItem :', selectedItem)
    db.transaction(tx => {
      if (!forDetail) {
        tx.executeSql(
          `select * from items`,
          [],
          (_, { rows: { _array } }) => setItems(_array)
        );
      } else {
        tx.executeSql(
          `select * from itemDetails where parentId=?`,
          [selectedItem ? selectedItem : 1],
          (_, { rows: { _array } }) => setItems(_array)
        );
      }
    });
  }, []);

  
  const addDetail = (text) => {
    if (text === null || text === "") {
      return false;
    }

    db.transaction(
      tx => {
        tx.executeSql(`insert into itemDetails (parentId, value) values (${selectedItem}, ?)`, [text]);
        tx.executeSql("select * from itemDetails", [], (_, { rows }) =>
          console.log(JSON.stringify(rows))
        );
      },
      null,
      null
    );

  }

  const heading = forDetail ? "Detail" : "Todo";

  if (items === null || items.length === 0) {
    return null;
  }

  return (
    <View style={styles.sectionContainer}>
      <Text style={styles.sectionHeading}>{heading}</Text>
      {items.map(({ id, done, value }) => (
        <TouchableOpacity
          key={id}
          onPress={() => {
            // onPressItem && onPressItem(id)
            // setIsSelected(id);
            onChangeSelected(id)
          }
          }
          style={{
            backgroundColor: selectedItem === id ? "#1c9963" : "#fff",
            borderColor: "#000",
            borderWidth: 1,
            padding: 8
          }}
        >
          <Text style={{ color: selectedItem === id ? "#fff" : "#000" }}>{value}</Text>
        </TouchableOpacity>
      ))}
      {heading === 'Todo' && <View style={styles.textContainer}>
        <TextInput
          onChangeText={text => setDetailText(text)}
          onSubmitEditing={() => {
            addDetail(detailText);
            setDetailText(null);
          }}
          placeholder="add detail for current item ?"
          style={styles.input}
          value={detailText}
        />
      </View>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    paddingTop: Constants.statusBarHeight
  },
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  sectionContainer: {
    marginBottom: 16,
    marginHorizontal: 16
  },
  sectionHeading: {
    fontSize: 18,
    marginBottom: 8
  },
  textContainer: {
    flexDirection: "row",
    backgroundColor: 'yellow',
    marginTop: 10
  },
});